#ifndef LQERROR_H
#define LQERROR_H

#include <QStringList>

class LQError {
public:
    static const LQError &Success();

    explicit LQError(int err_code, const QString &msg, const QString &type = QString{});
    LQError();
    LQError(const LQError &err);
    LQError &operator=(const LQError &other);

    /* обновление ошибки. Ошибка заменяется на err, если текущий
     * статус ОК, а err - не ОК. Используется для сохранения
     * первой возникшей ошибки */
    void update(const LQError &err);
    LQError &operator+=(const LQError &err);

    int errorCode() const {return m_err_code;}
    QString msg() const;
    const QString &type() const {return m_type;}
    void addMsgPrefix(const QString str);

    bool isSuccess() const {return isEqual(Success());}
    bool operator==(const LQError &rhs) const {return isEqual(rhs); }
    bool operator!=(const LQError &rhs) const {return !isEqual(rhs);}

    bool isEqual(const LQError &rhs) const {return (m_err_code == rhs.m_err_code) && (m_type == rhs.m_type);}
private:
    int m_err_code;
    QString m_type;
    QStringList m_msg;
};

#endif // LQERROR_H
