#include "LQError.h"

const LQError &LQError::Success() {
    static const LQError success_err{0, QString()};
    return  success_err;
}

LQError::LQError(int err_code, const QString &msg, const QString &type) :
    m_err_code{err_code}, m_type{type}, m_msg{msg} {

}

LQError::LQError() : LQError{Success()} {

}

LQError::LQError(const LQError &err) :
    m_err_code{err.m_err_code}, m_type{err.m_type}, m_msg{err.m_msg} {

}

LQError &LQError::operator=(const LQError &other) {
    if (this != &other) {
        m_err_code = other.m_err_code;
        m_type = other.m_type;
        m_msg = other.m_msg;
    }
    return *this;
}

void LQError::update(const LQError &err) {
    if (!err.isSuccess() && isSuccess())
        *this = err;
}

LQError &LQError::operator+=(const LQError &err) {
    update(err);
    return *this;
}

QString LQError::msg() const {
    QString ret;
    if (m_msg.size() == 1) {
        ret = m_msg.at(0);
    } else if (m_msg.size() > 1) {
        for (const QString &str : m_msg) {
            if (!ret.isEmpty())
                ret.append(QLatin1String{" "});
            ret += str;
        }
    }
    return ret;
}

void LQError::addMsgPrefix(const QString str) {
    m_msg.prepend(str);
}
